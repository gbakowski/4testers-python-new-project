from src.cars import Car

def test_initial_state_of_the_car():
    car = Car("Toyota", "black", 2003)
    assert car.brand == "Toyota"
    assert car.colour == "black"
    assert car.production_year == 2003
    assert car.mileage == 0

def test_drive_car_once():
    car = Car("Toyota", "black", 2003)
    car.drive(100)
    assert car.mileage == 100

def test_drive_car_twice():
    car = Car("Toyota", "black", 2003)
    car.drive(100)
    car.drive(100)
    assert car.mileage == 200

def test_colour_car_once():
    car = Car("Toyota", "black", 2003)
    car.repaint("black")
    assert car.colour == "black"