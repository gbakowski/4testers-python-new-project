from src.helpers import is_adult  # najpierw musimy zaimportować z głównego pliku to co chcemy testować


def test_is_not_adult_for_age_17(): # tutaj tworzymy logikę testowania poprzez funkcję oraz assert, któremu mówimy co ma potwierdzić na False lub True
    assert is_adult(17) == False
# możemy też tutaj zrobić zaprzeczenie
#assert not is_adult(17)

def test_is_adult_for_age_18():
    assert is_adult(18) == True


def test_is_adult_for_age_19():
    assert is_adult(19) == True
