class User: #tworzenie klasy dla progamowania obiektowego
    def __init__(self, email, age):
        self.email = email
        self.age = age

    def say_hello(self): #ta funkcja wywołuje jakiś opis dla instancji zapisanych w klasie
        print(f"Hi! My email is {self.email}")

    def person_age(self):
        print(f"My age is {self.age}")


if __name__ == '__main__':
    kate = User("kate@xample.com", 18)
    john = User("john@example.com", 42)

    print(john.age)
    print(kate.age)
    print(kate.email)
    print(john.email)

    kate.say_hello() # a tutaj wywołujemy funkcje (def say_hello), która ma zaimplemntowaną metodę dla klas - czyli aktywuje to co chcemy wywołać dla klass
    john.say_hello()

    kate.person_age()
    john.person_age()