class Car:
    def __init__(self, brand, colour, production_year):
        self.brand = brand
        self.colour = colour
        self.production_year = production_year
        self.mileage = 0

    def drive(self, distance):
        self.mileage += distance # dodaj do obecnej wart. self.mileage ten dystans lub "-=" odejmij


if __name__ == '__main__':
    car1 = Car("Toyota", "black", 2003)
    car2 = Car("Mazda", "red", 2023)
    car3 = Car("Opel", "white", 1991)

    print(car1.brand)
    print(car3.mileage)
